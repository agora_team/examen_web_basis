<?php

$save_inputs = [];

function init() {

    global $save_inputs;

    while($line = readline()) {
        $stripped = preg_replace('/\s+/', ' ', $line);
        $readed = explode(" ", $stripped);
        $save_inputs = array_merge($save_inputs, $readed);
    }
}

function read_word() {
    global $save_inputs;

    if(count($save_inputs) > 0) {
        return array_shift($save_inputs);
    }
    
    return false;
}

init();

//---code starts here ---\\

// you can use read_word function to read the next input
// ex : $next = read_word();

$next = read_word();
while($next !== false) {
    echo $next;
    echo PHP_EOL;
    $next = read_word();
}