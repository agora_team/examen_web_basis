<link rel="stylesheet" type="text/css" href="css/page_style.css">

<?php

require_once("db.php");

function print_container($data) {
  ?>
    <div class="container">
      <img src="/img/img_avatar2.png" alt="Avatar">
      <?php echo strtr('<p>content</p>', $data) ?>
      <?php echo strtr('<span class="time-right">date</span>', $data); ?>
    </div>
  <?php
}

function print_container_user($data) {
  ?>
    <div class="container darker">
      <img src="/img/img_avatar2.png" class="right" alt="Avatar">
      <?php echo strtr('<p>content</p>', $data) ?>
      <?php echo strtr('<span class="time-left">date</span>', $data); ?>
    </div>
  <?php
}

$posts = get_posts();

while($post = $posts->fetch_assoc()) {

  if($_SESSION['user'] == $post["id_user"]) {
    print_container_user($post);
  }
  else {
    print_container($post);
  }
  
}

?>
